# Overview

This repository contains the configuration and automation rules of my home automation system which is based on HomeAssistant. 

## Structure

* system configuration
* groups of entities
* scripts
* automation rules
* declarative UI

## Controllable entities

* heatings
* lights
* multimedia
* window sensors

## Features 

* presence detection and rules
* window-heating-rules
* low battery notifications
* vacation mode
* sunrise/sunset rules
* humidity rules
* Amazon Alexa integration
* Spotify integration

